# A cool project title
---
Give a few catch sentences on what it is your project does and why they should use it. Better yet use 
[shields](shields.io) so people can tell your project version, language, or the status of you CI system. This is your 
elevator pitch so try to get people hooked and wanting to read more.

![alt text](https://img.shields.io/npm/v/npm.svg "Shield1")  
![alt text](https://img.shields.io/teamcity/http/teamcity.jetbrains.com/s/bt345.svg "Shield2")  
![alt text](https://img.shields.io/pypi/v/nine.svg "Shield3")


## Installation
You want to make the installation as simple and standard as possible. Use package manager like `pip` and `npm` to make 
your project easy to install for people who want to just get up and running with this thing. Remember that everyone 
isn't a developer. 

## Getting started
Explain some of the simple tasks you can do with your library or tool. Where appropriate used `code blocks` to show how 
to use you classes and methods
```python
s = "Python syntax highlighting"
print s
```

## Contributing
If someone wants to add a feature of fix a bug what workflow do they needs to follow? Are there any developer tools that 
need to be installed to build and debug the code. Are there any unit test requirement? What about dependencies?

## Documentaion
Do you have additional documentation in a 
* WiKi
* Google Site
* Webpage generated with Sphinx? 
* Confluence

## Support
If people have questions where should they go

* Mailing list
* Slack group
* Google group
