.. TrimbleOpenSource documentation master file, created by
   sphinx-quickstart on Mon Nov 13 20:41:27 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Trimble Open Source!
===============================

Your go to source for all projects inside Trimble that have been developed to be shared, crowed sourced, and extensible
to meet your needs. The projects listed here follow an open source model in which the code is intended to be maintained
by one person or group but is open to the Trimble community to consume and contribute to. For more information on how to
develop a project that is intended to be Trimble open check out the Best Practices guide.

.. toctree::
   :maxdepth: 1

   best_practices

Hey! Where is my project?
-------------------------

Do you have a project you or your team has developed that you think others in Trimble could bennafit from? Is it not
listed here? Well, this page is also an open source project so fork it, make your changes, and then open a merge request
in the repo. We'll get is added as quickly as possible. Better yet, someone else from the community can review it and
recommend it be merged. Stay open my friends!

Trimble Open Source Projects
----------------------------

Here a list of *known* projects inside Trimble that were developed with a community first approach intended for others
to consume and contribute to. As this list grows, which we hope it does, we will consider breaking projects down by
domain or project type. In the meantime, here is the list!

+-----------------------------------+----------------------------------------------------------------------------------+
| Project                           | About                                                                            |
+===================================+==================================================================================+
| `Geodedic Library <https://sites  | The Trimble Geodetic Library (TGL) is a set of geodetic libraries commonly used  |
| .google.com/a/trimble.com/trimble | across Trimble groups and various applications.                                  |
| -geodetic-library-project/>`_     |                                                                                  |
+-----------------------------------+----------------------------------------------------------------------------------+


... TODO: check our `intersphinx` which will like between sphinx documentation of different projects