# Welcome to Trimble Source!
---

Your go to source for all projects inside Trimble that have been developed to be shared, crowed sourced, and extensible
to meet your needs. The projects listed here follow an open source model in which the code is intended to be maintained
by one person or group but is open to the Trimble community to consume and contribute to. For more information on how to
develop a project that is intended to be Trimble open check out the Best Practices guide.

* [Best Pratices](children/best_practices.md)
* [README Example](children/readme_example.md)

## Hey! Where is my project?
---

Do you have a project you or your team has developed that you think others in Trimble could bennafit from? Is it not
listed here? Well, this page is also an open source project so fork it, make your changes, and then open a merge request
in the repo. We'll get is added as quickly as possible. Better yet, someone else from the community can review it and
recommend it be merged. Stay open my friends!

## Trimble Source Projects
---
Here a list of *known* projects inside Trimble that were developed with a community first approach intended for others
to consume and contribute to. As this list grows, which we hope it does, we will consider breaking projects down by
domain or project type. In the meantime, here is the list!

| Project | Description |
| --------|-------------|
| [Geodedic Library](https://sites.google.com/a/trimble.com/trimble-geodetic-library-project/?pli=1) | The Trimble Geodetic Library (TGL) is a set of geodetic libraries commonly used across Trimble groups and various applications.|

## Limitation of BitBucket
---
The content here has been written in markdown because the current eTools instance of BitBucket does not support 
reStructuredText even though the cloud instance does. On the surface this limitation seems trivial but reStructuredText 
is more flexible than markdown, is natively supported in some programming languages like Python, and has a number of 
tools to support hosting  simple static web pages build in markdown like Sphinx. 

Unfortunately tools like [Sphinx](http://www.sphinx-doc.org/en/stable/index.html) and [Jekyll](https://jekyllrb.com/) 
that turn reStructuredText and Markdown into static html pages are not supported by the eTools BitBucket instance so 
for now we are stuck with simplistic markdown pages, which are acceptable for simple project, but by not stretch of the 
imagination engaging or exciting. The following list of pages highlights some of the possibilities of what static site 
hosting coupled with transformation tools like Sphinx and Jekyll can do. 

### Sites Built With Sphinx

* [This Document](https://ian_welch.gitlab.io/developer-guide/)
* [PyPa](https://pip.pypa.io/en/latest/)
* [PyCuda](https://documen.tician.de/pycuda/)

### Sites Built With Jekyll
* Need sources